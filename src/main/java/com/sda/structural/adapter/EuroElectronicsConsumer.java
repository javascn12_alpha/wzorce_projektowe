package com.sda.structural.adapter;

public class EuroElectronicsConsumer {
    private EuroDevice device;

    public EuroElectronicsConsumer(EuroDevice device) {
        this.device = device;
    }

    public void use() {
        System.out.println(EuroElectronicsConsumer.class.getSimpleName() + ": "
            + device.getClass().getSimpleName());
        device.plugInEuro();
        device.powerOn();
    }
}
