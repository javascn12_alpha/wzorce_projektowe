package com.sda.structural.adapter;

public interface UsDevice {
    void plugInUS();
    void powerOn();
}
