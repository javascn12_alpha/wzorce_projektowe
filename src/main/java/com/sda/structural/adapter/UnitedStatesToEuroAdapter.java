package com.sda.structural.adapter;

public class UnitedStatesToEuroAdapter implements EuroDevice {
    private UsDevice device;

    public UnitedStatesToEuroAdapter(UsDevice device) {
        this.device = device;
    }

    @Override
    public void plugInEuro() {
        device.plugInUS();
    }

    @Override
    public void powerOn() {
        device.powerOn();
    }
}
