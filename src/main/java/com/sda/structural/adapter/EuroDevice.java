package com.sda.structural.adapter;

public interface EuroDevice {
    void plugInEuro();
    void powerOn();
}
