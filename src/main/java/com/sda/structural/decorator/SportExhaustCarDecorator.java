package com.sda.structural.decorator;

public class SportExhaustCarDecorator extends CarDecorator {
    public SportExhaustCarDecorator(Car theCar) {
        super(theCar);
    }

    //sporty sound please!
    @Override
    public void start() {
        super.start();
        makeSportyNoise();
    }

    private void makeSportyNoise() {
        System.out.println("Wruuuuuuuuum!");
    }
}
