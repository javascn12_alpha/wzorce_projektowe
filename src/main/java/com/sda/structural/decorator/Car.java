package com.sda.structural.decorator;

public interface Car {
    void start();
    void turnLightsOn();
}
