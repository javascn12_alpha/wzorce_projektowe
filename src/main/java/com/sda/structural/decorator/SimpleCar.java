package com.sda.structural.decorator;

public class SimpleCar implements Car {

    @Override
    public void start() {
        System.out.println("brum...");
    }

    @Override
    public void turnLightsOn() {
        System.out.println("Turning lights on!");
    }
}
