package com.sda.structural.decorator;

public class AirConditionedCar extends CarDecorator {
    public AirConditionedCar(Car theCar) {
        super(theCar);
    }

    @Override
    public void start() {
        super.start();
        startAirConditioning();
    }

    private void startAirConditioning() {
        System.out.println("Air conditioning started");
    }
}
