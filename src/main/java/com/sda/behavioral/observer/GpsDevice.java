package com.sda.behavioral.observer;

import java.util.ArrayList;
import java.util.List;

public class GpsDevice {
    private List<GpsObserver> gpsObservers = new ArrayList<>();

    public interface GpsObserver {
        void onNewPosition(double latitude, double longitue);
    }

    public void getNePosition() {
        final double latitude = 50.0f;
        final double longitude = 25.0f;
        for (GpsObserver observer: gpsObservers) {
            observer.onNewPosition(latitude, longitude);
        }
    }
}
