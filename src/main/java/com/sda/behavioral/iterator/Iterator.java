package com.sda.behavioral.iterator;

public interface Iterator<E> {
    boolean hasNext();
    E next();
    void remove();
}
