package com.sda.behavioral.iterator;

public interface Iterable<T> {
    Iterator<T> iterator();
}
