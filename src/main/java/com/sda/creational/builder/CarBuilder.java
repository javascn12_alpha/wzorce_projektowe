package com.sda.creational.builder;

public interface CarBuilder {
    CarBuilder setEngineType(Car.EngineType engineType);
    Car build();
}
