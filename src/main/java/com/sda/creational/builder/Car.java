package com.sda.creational.builder;

public interface Car {
    enum EngineType {
        Combustion,
        Electric
    }
}
