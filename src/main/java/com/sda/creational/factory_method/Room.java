package com.sda.creational.factory_method;

public interface Room {
    void connect(Room room);
}
