package com.sda.creational.singleton;

/**
 * Singleton Billa Pugha - rozwiązuje problemy modelu pamięci w wersjach Javy < 5.
 * Przedstawiony jako ciekawostka.
 */
public class BillPughSingleton {

    private BillPughSingleton() {}

    private static class Holder {
        private static final BillPughSingleton instance = new BillPughSingleton();
    }

    public static BillPughSingleton getInstance() {
        return Holder.instance;
    }
}
