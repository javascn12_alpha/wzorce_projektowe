package com.sda.creational.singleton;

/**
 * Singleton leniwy - tworzy instancję podczas pierwszej próbu użycia.
 * Wada:
 *      1) nie jest bezpieczny w aplikacjach wielowątkowych
 *      2) możliwość uzyskania dostępu do konstruktora prywatnego przy użyciu refleksji
 */
public class LazySingleton {
    private static LazySingleton instance;

    private LazySingleton() {}

    public static LazySingleton getInstance() {
        if (instance == null) {
            instance = new LazySingleton();
        }
        return instance;
    }
}
