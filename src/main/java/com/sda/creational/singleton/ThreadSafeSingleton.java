package com.sda.creational.singleton;

/**
 * Singleton bezpieczny bezpieczny przy użyciu wielu wątków. Działa podobnie do singletona leniwego, ale tworzy swoją
 * instancję przy użyciu bloku synchronizowanego. Blok synchronizowany zabezpiecza przed sytuacją, kiedy dwa wątki
 * jednocześnie próbują wywołać konstruktor.
 *
 * Wada:
 *      1) koszt wydajnościowy użycia bloku synchronizowanego (zminimalizowane przez tzw. "double checked locking")
 *      2) możliwość uzyskania dostępu do konstruktora prywatnego przy użyciu refleksji
 */
public class ThreadSafeSingleton {
    private static ThreadSafeSingleton instance;

    private ThreadSafeSingleton() {}

    // Bez użycia "double checked locking".
    public static synchronized ThreadSafeSingleton getInstance_v1() {
        if (instance == null) {
            instance = new ThreadSafeSingleton();
        }
        return instance;
    }

    // Z użyciem "double checked locking".
    public static ThreadSafeSingleton getInstance_v2() {
        if (instance == null) {
            synchronized (ThreadSafeSingleton.class) {
                if (instance == null) {
                    instance = new ThreadSafeSingleton();
                }
            }
        }
        return instance;
    }
}
