package com.sda.creational.singleton;

/**
 * Singleton zachłanny - inicjalizacja następuje podczas startu programu.
 * Wada:
 *      1) może zostać zainicjalizowany chociaż nigdy nie będzie użyty
 *      2) brak możliwości obsługi wyjątków
 *      3) możliwość uzyskania dostępu do konstruktora prywatnego przy użyciu refleksji
 */
public class EagerSingleton {
    private static final EagerSingleton instance = new EagerSingleton();

    private EagerSingleton() {}

    public static EagerSingleton getInstance() {
        return instance;
    }
}
